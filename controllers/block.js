/* eslint-disable max-len */
const Block = require('../models/Block');

module.exports.add = async function(req, res) {
  const block = await Block.findOne({
    name: req.body.name,
  });

  if (block) {
    // Проверка на то что данный блок уже есть в базе
    res.render('main', {session: '', mass: [{text: 'Данный блок уже существует'}]});
  } else {
    const newBlock = new Block({
      name: req.body.name,
    });
    try {
      await newBlock.save();
      res.status(201).render('main', {session: '', mass: [{text: 'Блок создан'}]});
    } catch (e) {
      console.log(e);
    }
  }
};

module.exports.del = async function(req, res) {
  const block = await Block.findOne({
    name: req.body.name,
  }).catch((err) => console.log(err));
  console.log(block);
  if (block) {
    await Block.findOneAndDelete({
      name: req.body.name,
    }).catch((err) => console.log(err));
    res.status(200).render('main', {session: '', mass: [{text: 'Блок успешно удален'}]});
  } else {
    res.status(404).render('main', {session: '', mass: [{text: 'Блок не найден'}]});
  }
};
