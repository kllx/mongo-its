const JwtStrategy = require('passport-jwt').Strategy;
const User = require('../models/User');
const config = require('../libs/config');


const cookieExtractor = function(req) {
  let token = null;
  if (req.cookies) {
    token = req.cookies['Authorization'];
  }
  return token;
};

const options = {
  jwtFromRequest: cookieExtractor,
  secretOrKey: config.jwt,
};

module.exports = (passport) => {
  passport.use(
      new JwtStrategy(options, async (payload, done) => {
        try {
          const user = await User.findById(payload.userId).select('email id');

          if (user) {
            done(null, user);
          } else {
            done(null, false);
          }
        } catch (e) {
          console.log(e);
        }
      })
  );
};
