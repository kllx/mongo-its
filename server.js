const app = require('./app');
const config = require('./libs/config');
const port = config.port;

app.listen(port, () => {
  console.log(`Server started on ${port}`);
});
