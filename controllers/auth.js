const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken') // преположительно использовать barer jwt для логина
const User = require('../models/User')
const config = require('../libs/config')

module.exports.login = async function (req, res) {
  const candidate = await User.findOne({
    email: req.body.email
  })
  if (candidate) {
    // проверока пороля, пользователь существует
    const passwordResault = bcrypt.compareSync(req.body.pass, candidate.password)
    if (passwordResault) {
      // Генерация токена, пароли совпали
      const token = jwt.sign({
        email: candidate.email,
        userId: candidate._id
      }, config.jwt, { expiresIn: 60 * 60 })
      res.cookie('Authorization', `${token}`, {maxAge: 90000000, httpOnly: true})
      res.status(200).render('main', {session: candidate.name, mass: []} )
    } else {
      res.status(401).render('main', {session: '', mass:[{text:"Неверный пароль"}]})
    }
  } else {
    // Пользователя нет ошибка
    res.status(404).render('main', {session: '', mass:[{text:"Пользователь не найден"}]})
  }
}

module.exports.register = async function (req, res) {
  // email password
  const candidate = await User.findOne({ email: req.body.email })

  if (candidate) {
    // Пользователь существует нужно отдать ошибку
    res.status(409).json({
      message: 'Такой email уже занят. Попробуйте другой'
    })
  } else {
    // Нужно создать пользователя
    const salt = bcrypt.genSaltSync(10)
    const password = req.body.pass
    const user = new User({
      email: req.body.email,
      name: req.body.name,
      password: bcrypt.hashSync(password, salt)
    })

    try {
      await user.save()
      res.status(201).render('main', { session: 'Еще нет', mass:[{text:"А теперь залогинься"}]})
    } catch (e) {
      // Обработать ошибку
      console.log(e)
    }
  }

}