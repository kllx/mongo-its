const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const blockSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
});

module.exports = mongoose.model('blocks', blockSchema);
