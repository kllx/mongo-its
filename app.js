const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const multer = require('multer');
const path = require('path');
const passport = require('passport');
const config = require('./libs/config');
const cookieParser = require('cookie-parser');
const mainRoutes = require('./routes/index');
const mongoose = require('mongoose');

const app = express();
mongoose
    .connect(config.mongoose.uri, {useNewUrlParser: true, useCreateIndex: true})
    .then(() => console.log('Mongodb Connected'))
    .catch((err) => console.log(err));
app.use(cookieParser());
app.use(passport.initialize());
require('./middleware/passport')(passport);

app.use(compression());
app.use(require('morgan')('dev'));

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
  extended: false,
  limit: '50mb',
}));
app.use(multer(
    {
      dest: path.join(__dirname, 'public/uploads'),
      limits: {
        fieldNameSize: 999999999,
        fieldSize: 999999999,
      },
    }
).any());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
app.use('/files', express.static('../files'));

//* ************************ Routes ***********************************
app.use('/', mainRoutes);
//* ************************ 404 ***********************************
app.use(function(req, res) {
  res.locals.metatitle = '404 Ничего не найдено';
  res.locals.pagenoindex = 'yes';
  res.send('404 ошибка');
});
module.exports = app;
