/* eslint-disable new-cap */
const express = require('express');
const passport = require('passport');
const controllerAuth = require('../controllers/auth');
const controllerBlock = require('../controllers/block');
const router = express.Router();


router.get('/', async (req, res) => {
  res.render('main', {session: 'qwe', mass: [{text: 'Ты еще не вошел'}]});
});

router.post('/reg', controllerAuth.register);
router.post('/avtor', controllerAuth.login);
router.post('/add',
    passport.authenticate('jwt', {session: false}), controllerBlock.add);
router.post('/del',
    passport.authenticate('jwt', {session: false}), controllerBlock.del);

module.exports = router;
